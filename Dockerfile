FROM alpine:3.12

RUN echo -e "\
https://alpine.mirror.ac.za/v3.12/main\n\
https://alpine.mirror.ac.za/v3.12/community" > /etc/apk/repositories

RUN apk --no-cache add p7zip curl git unzip outils-rs sed rsync openssh-client tree bash
